#include <iostream>
#include <cstdio>
#include <sstream>
#include <algorithm>
#include <vector>
#define pow2(n) (1 << (n))
using namespace std;
 
 
// Node Declaration
 
struct avl_node
{
    int data;
    std::string name;
    struct avl_node *left;
    struct avl_node *right;
};
 
 
// Class Declaration
 
class avlTree
{
    public:
        int height(avl_node *);
        int diff(avl_node *);
        avl_node *rr_rotation(avl_node *);
        avl_node *ll_rotation(avl_node *);
        avl_node *lr_rotation(avl_node *);
        avl_node *rl_rotation(avl_node *);
        avl_node* balance(avl_node *);
        avl_node* insert(avl_node *,std::string, int );
        std::vector<std::string> inOrder(avl_node*);
        std::vector<std::pair<std::string, int> > inOrderPair(avl_node*);
        void inorder(avl_node *);
        void destroy(avl_node*);
        avlTree();
};
