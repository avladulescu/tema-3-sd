/* Copyright 2017 Tilica Dora Nicoleta si Vladulescu Alexandru Valentin*/

#include <iterator>
#include <string>
#include <cstring>
#include <vector>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <map>
#include <utility>
#include <list>
#include "include/imdb.h"

struct avl_node *radacina;
struct avl_node *radacina_rating;
bool sort_func(std::pair<std::string, int> i, std::pair<std::string, int> j){
	return i.second == j.second && i.first > j.first;
}
std::string firstElement(const std::pair<std::string, int> &p) {
    return p.first;
}

std::string Timestamp_to_year(int timestamp){
	time_t t = timestamp;
	struct tm *timp;
	timp = new struct tm;
	gmtime_r(&t, timp);
	char date[20];
	strftime(date, sizeof(date), "%Y-%m-%d", timp);
	char *p, *paux;
	p = strtok_r(date, "-", &paux);
	delete timp;
	return p;
}
IMDb::IMDb() {
    Movies = new std::list<struct Movie> [HMAX];
    Users = new std::list<struct User> [2*HMAX];
    Actors = new std::list<struct Actor> [HMAX];
}

IMDb::~IMDb() {
	for(int i = 0; i < HMAX; i++){
        while (!Movies[i].empty()){
            Movies[i].pop_front();
        }
        while (!Users[i].empty()){
            Users[i].pop_front();
        }
        while (!Actors[i].empty()){
            Actors[i].pop_front();
        }
    }
    delete[] Movies;
    delete[] Actors;
    delete[] Users;
}

void IMDb::add_movie(std::string movie_name,
                     std::string movie_id,
                     int timestamp,  // unix timestamp when movie was launched
                     std::vector<std::string> categories,
                     std::string director_name,
                     std::vector<std::string> actor_ids) {
	struct Movie info;
    int hkey = hash(movie_id)%HMAX;
    typename std::list<struct Movie>::iterator it;
    for (it = Movies[hkey].begin(); it != Movies[hkey].end(); it++){
        if (it->movie_id == movie_id) break;
    }
    if (it != Movies[hkey].end()){
        it->movie_name = movie_name;
        it->timestamp = timestamp;
        it->categories = categories;
        it->director_name = director_name;
        it->actor_ids = actor_ids;

	    // adaugare cel mai mic si cel mai mare timestamp in actors
		for ( unsigned int i = 0 ; i < it->actor_ids.size() ; i++ ){
		    for ( unsigned int j = 0 ; j < actors.size() ; j++ ){
		    	if ( it->actor_ids[i] == actors[j].first ){
					if ( it->timestamp <
					 actors[j].second.first ){
						actors[j].second.first
						 = it->timestamp;
					} else if (it->timestamp >
					 actors[j].second.second){
						actors[j].second.second
						 = it->timestamp;
					}
					goto LOOP1;
				}
		    }
		    actors.push_back(std::make_pair(it->actor_ids[i],
		    std::make_pair(it->timestamp, it->timestamp) ));
			LOOP1: {}
		}
	        it->rating = 0;
    } else {
        info.movie_name = movie_name;
        info.movie_id = movie_id;
        info.timestamp = timestamp;
        info.categories = categories;
        info.director_name = director_name;
        info.actor_ids = actor_ids;
	for ( unsigned int i = 0 ; i < info.actor_ids.size() ; i++ ){
	    for ( unsigned int j = 0 ; j < actors.size() ; j++ ){
	    	if ( info.actor_ids[i] == actors[j].first ){
				if ( info.timestamp < actors[j].second.first ){
					actors[j].second.first =
					 info.timestamp;
				} else if (info.timestamp >
				 actors[j].second.second){
					actors[j].second.second
					 = info.timestamp;
				}
				goto LOOP2;
			}
	    }
	    actors.push_back(std::make_pair(info.actor_ids[i],
	    std::make_pair(info.timestamp, info.timestamp) ));
	    LOOP2: {}
	}
        info.rating = 0;
        Movies[hkey].push_back(info);
    }

    // adaugare actori pentru fiecare director

	if (directors.size() > 0){
		for (unsigned int i = 0; i < directors.size(); i++){
	    	if (director_name == directors[i].first){
	    		unsigned int lung = directors[i].second.size();
	    		for (unsigned int j = 0; j < actor_ids.size(); j++) {
	    			for (unsigned int l = 0; l < lung; l++){
	    				if (actor_ids[j] ==
	    				 directors[i].second[l]) {
	    					goto LOOP4;
	    				}
	    			}
	    			directors[i].second.push_back(actor_ids[j]);
	    			LOOP4: {}
	    		}
	    		goto LOOP3;
	    	}
	    }
	    directors.push_back(std::make_pair(director_name, actor_ids));
	    LOOP3: {}
	} else {
		directors.push_back(std::make_pair(director_name, actor_ids));
	}
	// adaugare actorii si colegii sai
	for ( unsigned int i = 0 ; i < actor_ids.size() ; i++ ){
		int hkey1 = hash(actor_ids[i])%HMAX;
		typename std::list<struct Actor>::iterator it1;
		for ( it1 = Actors[hkey1].begin() ; it1 !=
		 Actors[hkey1].end() ; it1++ ){
			if ( it1->actor_id == actor_ids[i] ) break;
		}
		if ( it1 != Actors[hkey1].end() ){
			for (unsigned int j = 0 ; j < actor_ids.size() ; j++){
				if ( it1->actor_id != actor_ids[j] ){
					std::vector<std::pair<std::string, int>
					>::iterator it2;
					int ok = 0;
					for (it2 = it1->colleagues.begin(); it2
					 != it1->colleagues.end(); it2++) {
						if (it2->first ==
						actor_ids[j]) {
							// it2->second++;
							ok = 1;
							break;
						}
					}
					if (ok == 0) {
					it1->colleagues.push_back
					(std::make_pair(actor_ids[j], 0));
					}


					// it1->nr_movies.push_back(1);
				}
			}
			for (unsigned int j = 0; j < it1->colleagues.size();
			 j++){
				for ( unsigned int k = 0 ; k < actor_ids.size() ;
				 k++ ){
					if ( it1->colleagues[j].first ==
					actor_ids[k] ){
						it1->colleagues[j].second++;
					}
				}
			}
		}
	}
	// adaugare movie_id si timestamp in tree
		radacina = tree.insert(radacina, movie_id, timestamp);
    // adaugare movie_id in movie_ids
	movie_ids.push_back(movie_id);
  }

void IMDb::add_user(std::string user_id, std::string name) {
	struct User info;
	int hkey = hash(user_id)%HMAX;
	typename std::list<struct User>::iterator it;
	for ( it = Users[hkey].begin() ; it != Users[hkey].end() ; it++ ){
		if ( it->user_id == user_id ) break;
	}
	if ( it != Users[hkey].end() ){
		it->name = name;
	} else {
		info.user_id = user_id;
		info.name = name;
		Users[hkey].push_back(info);
	}
}
void IMDb::add_actor(std::string actor_id, std::string name) {
	struct Actor info;
	int hkey = hash(actor_id)%HMAX;
	typename std::list<struct Actor>::iterator it;
	for ( it = Actors[hkey].begin() ; it != Actors[hkey].end() ; it++ ){
		if ( it->actor_id == actor_id ) break;
	}
	if ( it != Actors[hkey].end() ){
		it->name = name;
	} else {
		info.actor_id = actor_id;
		info.name = name;
		Actors[hkey].push_back(info);
	}
}
void IMDb::add_rating(std::string user_id, std::string movie_id, int rating) {
	int hkey = hash(movie_id)%HMAX;
    typename std::list<struct Movie>::iterator it;
    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end() ; it++ ){
        if ( it->movie_id == movie_id ) break;
    }
    if ( it != Movies[hkey].end() ){
    	for ( unsigned int i = 0 ; i < it->ratings.size() ; i++ ){
			if ( it->ratings[i].first == user_id )
			    return;
    	}
		it->ratings.push_back(std::make_pair(user_id, rating));
    }
}
void IMDb::update_rating(std::string user_id, std::string movie_id,
                         int rating) {
	int hkey = hash(movie_id)%HMAX;
    typename std::list<struct Movie>::iterator it;
    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end() ; it++ ){
        if ( it->movie_id == movie_id ) break;
    }
    if ( it != Movies[hkey].end() ){
        for (unsigned int i = 0; i < it->ratings.size(); i++){
        	if ( it->ratings[i].first == user_id ){
        		it->ratings[i].second = rating;
        	}
        }
    }
}
void IMDb::remove_rating(std::string user_id, std::string movie_id) {
	int hkey = hash(movie_id)%HMAX;
    typename std::list<struct Movie>::iterator it;
    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end() ; it++ ){
        if ( it->movie_id == movie_id ) break;
    }
    if ( it != Movies[hkey].end() ){
        for (unsigned int i = 0; i < it->ratings.size(); i++){
        	if ( it->ratings[i].first == user_id ){
        		it->ratings.erase(it->ratings.begin() + i);
        	}
        }
    }
}
std::string IMDb::get_rating(std::string movie_id) {
	int hkey = hash(movie_id)%HMAX;
    typename std::list<struct Movie>::iterator it;
    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end() ; it++ ){
        if ( it->movie_id == movie_id ) break;
    }
    if ( it->ratings.size() == 0 ) return "none";
    if ( it != Movies[hkey].end() ){
    	double s = 0;
        for (unsigned int i = 0; i < it->ratings.size(); i++){
        	s += it->ratings[i].second;
        }
    	double round_m = s/it->ratings.size();
		it->rating = round_m;
		std::stringstream out;
		out << std::fixed << std::setprecision(2) << round_m;
        return out.str();
    }
    return "none";
}
std::string IMDb::get_rating_not_round(std::string movie_id) {
	int hkey = hash(movie_id)%HMAX;
    typename std::list<struct Movie>::iterator it;
    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end() ; it++ ){
        if ( it->movie_id == movie_id ) break;
    }
    if ( it->ratings.size() == 0 ) return "none";
    if ( it != Movies[hkey].end() ){
    	double s = 0;
        for (unsigned int i = 0; i < it->ratings.size(); i++){
        	s += it->ratings[i].second;
        }
        double round_m = s/it->ratings.size();
	std::stringstream out;
	out << round_m;
        return out.str();
    } else {
    	return "none";
    }
    return "none";
}
std::string IMDb::get_longest_career_actor() {
    std::string max_id;
    int longevity = -1;
    if (actors.size() > 0){
    	for ( unsigned int i = 0 ; i < actors.size() ; i++ ){
			if (longevity < (actors[i].second.second
			 - actors[i].second.first)) {
				longevity = actors[i].second.second
				 - actors[i].second.first;
				max_id = actors[i].first;
			} else {
				if (longevity == (actors[i].second.second
				 - actors[i].second.first)) {
					if (max_id.compare(actors[i].first)
					 > 0){
						max_id = actors[i].first;
					}
				}
			}
	    }
	    return max_id;
	}
    return "none";
}
std::string IMDb::get_most_influential_director() {
    std::string best_director = "none";
	unsigned int max_actors = 0;
	for (unsigned int i = 0; i < directors.size(); i++) {
		if (max_actors < directors[i].second.size()) {
			max_actors = directors[i].second.size();
			best_director = directors[i].first;
		} else {
			if (max_actors == directors[i].second.size()) {
				if (best_director > directors[i].first) {
					best_director = directors[i].first;
				}
			}
		}
	}
    return best_director;
}

std::string IMDb::get_best_year_for_category(std::string category) {
	std::vector<std::pair<std::string, double> > rating_ani;
	std::vector<int> contor;
    std::string out = "none";
    for ( unsigned int i = 0 ; i < movie_ids.size() ; i++ ){
		int hkey = hash(movie_ids[i])%HMAX;
	    typename std::list<struct Movie>::iterator it;
	    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end(); it++ ){
	    	if ( it->movie_id == movie_ids[i] ) break;
	    }
    	if ( it != Movies[hkey].end() ){
    		std::string time = Timestamp_to_year(it->timestamp);
    		std::string rating_c = IMDb::get_rating(movie_ids[i]);
    		if (rating_c != "none") {
    			for ( unsigned int j = 0 ; j < it->categories.size();
    			j++ ){
	    			if (it->categories[j] == category){
	    				int ok = 0;
	    				for ( unsigned int k = 0; k <
	    				 rating_ani.size(); k++) {
	    					if (rating_ani[k].first
	    					 == time) {
	    						rating_ani[k].second +=
	    						 atoi(rating_c.c_str());
	    						contor[k]++;
	    						ok = 1;
	    						break;
	    					}
	    				}
	    				if (ok == 0) {
	    					rating_ani.push_back
	    					(std::make_pair(time,
	    					 atoi(rating_c.c_str())));
	    					contor.push_back(1);
	    				}
	    			}
    			}
    		}
		}
    }
    double max_rating = -1;
    for (unsigned int i = 0; i < rating_ani.size(); i++) {
    	rating_ani[i].second = rating_ani[i].second / contor[i];
    	if (max_rating < rating_ani[i].second) {
    		max_rating = rating_ani[i].second;
    		out = rating_ani[i].first;
    	} else {
    		if (max_rating == rating_ani[i].second) {
    			if (rating_ani[i].first < out) {
    				out = rating_ani[i].first;
    			}
	    	}
    	}
    }
    return out;
}
std::string IMDb::get_2nd_degree_colleagues(std::string actor_id) {
    std::map<std::string, int> viz;
    std::vector<std::string> second_dc;	 // Second degree colleagues
    viz[actor_id] = 1;
    int hkey = hash(actor_id)%HMAX;
    typename std::list<struct Actor>::iterator it;
    for ( it = Actors[hkey].begin() ; it != Actors[hkey].end() ; it++ ){
		if ( it->actor_id == actor_id ) {
			break;
		}
    }
    if ( it != Actors[hkey].end() ) {
    	if ( it->colleagues.size() == 0 )
    			return "none";
		for ( unsigned int i = 0 ; i < it->colleagues.size() ; i++ ){
	   		viz[it->colleagues[i].first] = 1;
		}
		for ( unsigned int i = 0 ; i < it->colleagues.size() ; i++ ) {
			typename std::list<struct Actor>::iterator it1;
			 int hkey1 = hash(it->colleagues[i].first)%HMAX;
			for (it1 = Actors[hkey1].begin(); it1 !=
			 Actors[hkey1].end(); it1++) {
				if ( it1->actor_id ==
				 it->colleagues[i].first) break;
		    }
		    if ( it1 != Actors[hkey1].end() ) {
		    	for ( unsigned int j = 0 ; j < it1->colleagues.size()
		    	 ; j++ ) {
		    		if ( viz[it1->colleagues[j].first] != 1 )
						second_dc.push_back
						(it1->colleagues[j].first);
						viz[it1->colleagues[j].first]
						 = 1;
		    	}
		    }
		}
    }
    std::sort(second_dc.begin(), second_dc.end());
    if ( !second_dc.empty() ) {
		std::ostringstream str;
		std::copy(second_dc.begin(), second_dc.end() - 1,
			std::ostream_iterator< std::string > (str, " "));
		std::copy(second_dc.end() - 1, second_dc.end(),
			 std::ostream_iterator< std::string > (str, ""));
		std::vector<std::string>::iterator it2;
		return str.str();
    } else {
    	return "none";
    }
}
std::string IMDb::get_top_k_most_recent_movies(int k) {
	std::vector<std::string> inOrderTree = tree.inOrder(radacina);
	if (inOrderTree.size() == 0) return "none";
	std::stringstream res;
	if ((unsigned int)k > inOrderTree.size()){
		std::copy(inOrderTree.rbegin(), inOrderTree.rend()-1,
		 std::ostream_iterator<std::string>(res, " "));
		std::copy(inOrderTree.rend()-1, inOrderTree.rend(),
		 std::ostream_iterator<std::string>(res, ""));
	} else {
		std::copy(inOrderTree.rbegin(), inOrderTree.rbegin() + k-1,
		 std::ostream_iterator<std::string>(res, " "));
		std::copy(inOrderTree.rbegin()+k-1, inOrderTree.rbegin() + k,
		 std::ostream_iterator<std::string>(res, ""));
	}
    return res.str();
}
bool sort_pair(struct ActorPair i, struct ActorPair j){
	return i.nr > j.nr ||
		 (i.nr == j.nr && i.name1 < j.name1) ||
		 (i.nr == j.nr && i.name1 == j.name1 && i.name2 < j.name2);
}
bool SamePair(struct ActorPair i, struct ActorPair j){
    return i.nr == j.nr && i.name1 == j.name1 && i.name2 == j.name2;
}
std::string IMDb::get_top_k_actor_pairs(int k) {
	std::vector<struct ActorPair> res;
	for ( unsigned int i = 0 ; i < actors.size() ; i++ ){
		int hkey = hash(actors[i].first)%HMAX;
		typename std::list<struct Actor>::iterator it;
		for ( it = Actors[hkey].begin() ; it != Actors[hkey].end()
		 ; it++ ){
			if ( it->actor_id == actors[i].first ) break;
		}
		if ( it != Actors[hkey].end() ){
		    for ( unsigned int j = 0 ; j < it->colleagues.size()
		     ; j++ ){
				if ( it->actor_id < it->colleagues[j].first ) {
					struct ActorPair aux;
					aux.name1 = it->actor_id;
					aux.name2 = it->colleagues[j].first;
					aux.nr = it->colleagues[j].second;
					res.push_back(aux);
				} else if ( it->actor_id >
				it->colleagues[j].first ) {
					struct ActorPair aux;
					aux.name1 = it->colleagues[j].first;
					aux.name2 = it->actor_id;
					aux.nr = it->colleagues[j].second;
					res.push_back(aux);
				}
	   		}
		}
	}
	std::sort(res.begin(), res.end(), sort_pair);
	res.erase(std::unique(res.begin(), res.end(), SamePair), res.end());
	if (res.size() == 0)
		return "none";
	std::stringstream sol;
	if ((unsigned int)k >= res.size()){
		for ( unsigned int i = 0 ; i < res.size(); i++ ){
			sol << "(" << res[i].name1 << " " << res[i].name2
			    << " " << res[i].nr << ")";
			if ( i < res.size() - 1)
				sol << " ";
		}
	} else {
		for ( unsigned int i = 0 ; i < (unsigned)k ; i++ ){
			sol << "(" << res[i].name1 << " " << res[i].name2
			    << " " << res[i].nr << ")";
			if ( i < (unsigned)k - 1)
				sol << " ";
		}
	}
    	return sol.str();
}
bool sort_pair2(struct ActorPair i, struct ActorPair j){
	return i.nr > j.nr || ( i.nr == j.nr && i.name2 < j.name2);
}
std::string IMDb::get_top_k_partners_for_actor(int k, std::string actor_id) {
    std::vector<struct ActorPair> res;
	int hkey = hash(actor_id)%HMAX;
	typename std::list<struct Actor>::iterator it;
	for ( it = Actors[hkey].begin() ; it != Actors[hkey].end() ; it++ ){
		if ( it->actor_id == actor_id ) {
			break;
		}
	}
	if ( it != Actors[hkey].end() ){
	    for ( unsigned int j = 0 ; j < it->colleagues.size() ; j++ ){
			struct ActorPair aux;
			aux.name1 = it->actor_id;
			aux.name2 = it->colleagues[j].first;
			aux.nr = it->colleagues[j].second;
			res.push_back(aux);
		}
	}
	std::sort(res.begin(), res.end(), sort_pair2);
	res.erase(std::unique(res.begin(), res.end(), SamePair), res.end());
	if (res.size() == 0)
		return "none";
	std::stringstream sol;
	if (res.size() == 1) {
		return res[0].name2;
	}
	if ((unsigned int)k >= res.size()){
		for ( unsigned int i = 0 ; i < res.size() ; i++ ){
			sol << res[i].name2;
			if ( i < res.size() - 1 )
				sol << " ";
		}
	} else {
		for ( unsigned int i = 0 ; i < (unsigned)k; i++ ){
			sol << res[i].name2;
			if ( i < (unsigned)k - 1)
				sol << " ";
		}
	}
    return sol.str();
}

bool Empty(const std::string& str)
{
    return str.empty();
}
std::string IMDb::get_top_k_most_popular_movies(int k) {
	radacina_rating = new struct avl_node();
	for ( unsigned int i = 0 ; i < movie_ids.size() ; i++ ){
		int hkey = hash(movie_ids[i])%HMAX;
	    typename std::list<struct Movie>::iterator it;
	    for ( it = Movies[hkey].begin() ; it != Movies[hkey].end() ; it++ ){
	    	if ( it->movie_id == movie_ids[i] ) break;
	    }
    	if ( it != Movies[hkey].end() ){
		    radacina_rating = tree_rating.insert(radacina_rating,
		     movie_ids[i], it->ratings.size());
		}
    }
    std::vector<std::pair<std::string, int> > inOrderTree =
     tree_rating.inOrderPair(radacina_rating);
	if (inOrderTree.size() == 0) return "none";
	std::stringstream res;
	std::sort(inOrderTree.begin(), inOrderTree.end(), sort_func);
	std::vector<std::string> item;
	std::transform(inOrderTree.rbegin(), inOrderTree.rend(),
	std::back_inserter(item), firstElement);
	item.erase(std::remove_if(item.begin(), item.end(), Empty), item.end());
	if ((unsigned int)k >= item.size()){
		std::copy(item.begin(), item.end()-1,
		 std::ostream_iterator<std::string>(res, " "));
		std::copy(item.end()-1, item.end(),
		 std::ostream_iterator<std::string>(res, ""));
	} else {
		std::copy(item.begin(), item.begin()+k-1,
		 std::ostream_iterator<std::string>(res, " "));
		std::copy(item.begin()+k-1, item.begin()+k,
		 std::ostream_iterator<std::string>(res, ""));
	}
	tree_rating.destroy(radacina_rating);
    	return res.str();
}
std::string IMDb::get_avg_rating_in_range(int start, int end) {
    double rez = 0;
    int count = 0;
    for ( unsigned int i = 0 ; i < movie_ids.size() ; i++ ){
	int hkey = hash(movie_ids[i])%HMAX;
	typename std::list<struct Movie>::iterator it;
    	for ( it = Movies[hkey].begin() ; it != Movies[hkey].end();
    	 it++ ){
	    if (it->movie_id == movie_ids[i] && it->timestamp >=
	     start && it->timestamp <= end) {
	     	break;
	     }
    	}
	if ( it->ratings.size() != 0 )
    		if ( it != Movies[hkey].end() ){
			double nr = 0;
			for ( unsigned int i = 0 ; i < it->ratings.size()
			 ; i++ ){
				nr += it->ratings[i].second;
			}
			rez += nr/it->ratings.size();
			count++;
		}
    }
    std::string res = "none";
    if ( rez > 0 ){
	std::stringstream out;
	out << std::fixed << std::setprecision(2) << rez/count;
	res = out.str();
    }
    return res;
}
