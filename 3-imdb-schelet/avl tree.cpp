#include "include/avl tree.h"
struct avl_node *root;
avlTree::avlTree(){
    root = NULL;
}

  // Height of AVL Tree

int avlTree::height(avl_node *temp)
{
    int h = 0;
    if (temp != NULL)
    {
        int l_height = height (temp->left);
        int r_height = height (temp->right);
        int max_height = max (l_height, r_height);
        h = max_height + 1;
    }
    return h;
}
 
 
  // Height Difference
 
int avlTree::diff(avl_node *temp)
{
    int l_height = height (temp->left);
    int r_height = height (temp->right);
    int b_factor= l_height - r_height;
    return b_factor;
}
 
 
  // Right- Right Rotation
 
avl_node *avlTree::rr_rotation(avl_node *parent)
{
    avl_node *temp;
    temp = parent->right;
    parent->right = temp->left;
    temp->left = parent;
    return temp;
}
 
  // Left- Left Rotation
 
avl_node *avlTree::ll_rotation(avl_node *parent)
{
    avl_node *temp;
    temp = parent->left;
    parent->left = temp->right;
    temp->right = parent;
    return temp;
}
 
 
  // Left - Right Rotation
 
avl_node *avlTree::lr_rotation(avl_node *parent)
{
    avl_node *temp;
    temp = parent->left;
    parent->left = rr_rotation (temp);
    return ll_rotation (parent);
}
 
 
  // Right- Left Rotation
 
avl_node *avlTree::rl_rotation(avl_node *parent)
{
    avl_node *temp;
    temp = parent->right;
    parent->right = ll_rotation (temp);
    return rr_rotation (parent);
}
 
 
  // Balancing AVL Tree
 
avl_node *avlTree::balance(avl_node *temp)
{
    int bal_factor = diff (temp);
    if (bal_factor > 1)
    {
        if (diff (temp->left) > 0)
            temp = ll_rotation (temp);
        else
            temp = lr_rotation (temp);
    }
    else if (bal_factor < -1)
    {
        if (diff (temp->right) > 0)
            temp = rl_rotation (temp);
        else
            temp = rr_rotation (temp);
    }
    return temp;
}
 
 
  // Insert Element into the tree
 
avl_node *avlTree::insert(avl_node *root, std::string name, int value)
{
    if (root == NULL)
    {
        root = new avl_node;
        root->name = name;
        root->data = value;
        root->left = NULL;
        root->right = NULL;
        return root;
    }
    else if (value < root->data)
    {
        root->left = insert(root->left, name, value);
        root = balance (root);
    }
    else if (value >= root->data)
    {
        root->right = insert(root->right, name, value);
        root = balance (root);
    }
    return root;
} 
  // Inorder Traversal of AVL Tree
 
std::vector<std::string> avlTree::inOrder(avl_node *tree){
    std::vector<std::string> r;
    if (tree != NULL){
        std::vector<std::string> x;
        x = inOrder(tree -> left);
        r.insert(r.end(), x.begin(), x.end());
        r.push_back(tree->name);
        x = inOrder(tree -> right);
        r.insert(r.end(), x.begin(), x.end());

    }
    return r;
}
std::vector<std::pair<std::string, int> > avlTree::inOrderPair(avl_node *tree){
    std::vector<std::pair<std::string, int> > r;
    if (tree != NULL){
        std::vector<std::pair<std::string, int> > x;
        x = inOrderPair(tree -> left);
        r.insert(r.end(), x.begin(), x.end());
        r.push_back(std::make_pair(tree->name, tree->data));
        x = inOrderPair(tree -> right);
        r.insert(r.end(), x.begin(), x.end());

    }
    return r;
}
void avlTree::inorder(avl_node *tree)
{
    if (tree == NULL)
        return;
    inorder (tree->left);
    std::cout<< tree->name << " " << tree->data<<std::endl;
    inorder (tree->right);
}
void avlTree::destroy(avl_node *tree){
    if (tree != NULL) {
        destroy(tree->left);
        destroy(tree->right);
        delete tree;
    }
}
